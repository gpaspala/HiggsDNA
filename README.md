# HiggsDNA  
HiggsDNA (Higgs to Diphoton NanoAOD Framework) is the tool used for Higgs to diphoton related analyses in Run3. The updated and maintained documentation is available [here](https://higgs-dna.readthedocs.io/en/latest/index.html#).

This fork is developped on HiggsDNA for the HH-\gtbbgg analysis.


# Basic principle

A Tuto was given for this fork of HiggsDNA [here](https://indico.cern.ch/event/1360961/contributions/5777678/attachments/2788218/4861762/HiggsDNA_tutorial.pdf) . It is strongly recommended to check the official documentation, as this tuto does not describe all the ways to use HiggsDNA.

## Command line 

Basic command line : 
`python run_analysis.py --json-analysis YourJson.json --dump output_test `

Command line "ready to go ". In the `tests/` directory , run : 

`python ../scripts/run_analysis.py --json-analysis test.json  --dump output_test --skipJetVetoMap --skipCQR --voms PathToYourCertificate`

`--skipCQR` allows you to skip the Chained Quantile Regression, which needs files that are not automatically downloaded. 
`--skipJetVetoMap` The files to apply the jet veto are also not yet automatically in the HiggsDNA repository. 
`--voms PathToYourCertificate`  This option is necessary if you want to access the files using xrootd (AAA)

## Worfklow 

The principle is based on worflows that can me found in `higgsdna/worfkows/`. This is where the systematics, the scale factors, MVA, and selection cuts are applied. The base workflow used for the Hgg analysis is `base.py` . the `HHbbgg_base.py`processor is built from the `base.py`one and for the bbgg analysis. The cuts and the variables that are defined for now are detailed below. 
`HHbbgg_studies.py` is built on the same principle as the `dystudies.py`. It defines some variable needed to make things work ( analysis name, trigger ...). 

NB : "base" processors are a "hollow" function, so it can't be run directly in run_analysis as it is, hence the use of two scripts. There may be a better way to do it, but it works fine as it is. 

### New worklow with inheritance 

`HHbbgg.py`is a worflow inheriting directly from the `base`processor (main processor for Hgg analysis). All the functions defined in the `base` worflow are directly inherited , and don't need do be re-impleneted in `HHbbgg`, except the process function.


`higgs_dna/workflows/__init__.py` is where the processors are defined with names to be recognized by the `run_analysis.py`. 

## Json files 

As it can be seen in the command line, the `run_analysis.py` script need a json file (`test.json`). here is an example of such a file : ( An other example of such a file can be found in `tests/test.json` )

```
{
    "samplejson": "samples_v12_test.json",
    "workflow": "HHbbgg",
    "metaconditions": "Era2018_legacy_v1",
    "year": {
        "GluGluToGG": [
            "2022postEE"
        ],
        "VBFHToGG": [
            "2022postEE"
        ],
        "ttHToGG": [
            "2022postEE"
        ],
        "VHToGG": [
            "2022postEE"
        ],
        "GJetPt20To40": [
            "2022postEE"
        ],
        "GjetPt40": [
            "2022postEE"
        ],
        "GGJets": [
            "2022postEE"
        ]
    },
    "taggers": [],
    "systematics": {
            "JES"
                },
    "corrections": {
          },
    "analysis": "mainAnalysis"
}
```
* `sample_v12_test.json` is a list of samples you want to use. example here : 

```
{
    "GluGluToGG": [
        "/eos/lyoeos.in2p3.fr/grid/cms/store/mc/Run3Summer22EENanoAODv12/GluGluHtoGG_M-125_TuneCP5_13p6TeV_amcatnloFXFX-pythia8/NANOAODSIM/130X_mcRun3_2022_realistic_postEE_v6-v2/2520000/ef675733-95e7-4432-9555-8b8255eadaa3.root",
        "/eos/lyoeos.in2p3.fr/grid/cms/store/mc/Run3Summer22EENanoAODv12/GluGluHtoGG_M-125_TuneCP5_13p6TeV_amcatnloFXFX-pythia8/NANOAODSIM/130X_mcRun3_2022_realistic_postEE_v6-v2/2530000/8ac8d687-3e17-4795-98c2-0c4becc7bd33.root"
    ],
     "VBFHToGG": [
        "/eos/lyoeos.in2p3.fr/grid/cms/store/mc/Run3Summer22EENanoAODv12/VBFHtoGG_M-125_TuneCP5_13p6TeV_amcatnlo-pythia8/NANOAODSIM/130X_mcRun3_2022_realistic_postEE_v6-v2/50000/3f47d863-fa69-4c13-9892-90b097850462.root"
    ]
}
```
* `metaconditions`

* `systematics` and `corrections`

In the same Json file, systematics can be added. These systematics are defined in `higgsdna/systematics/` and the names that should be used to call them are defined in `__init__.py`. 

If you want to include sysmtematics or corrections requiring a json file not yet present in the systemtics directory, use `scripts/pull_files.py` :

`python pull_files.py --target WhichSytematicYouWant` or use the option `--all` to download everything. 

# Cuts

## $H\longrightarrow \gamma \gamma$

The main cuts applied in the Hgg analysis are also applied in bbgg analysis and are : 

*  $p_T^{lead photon}\gt 35 GeV$
*  $p_T^{sublead photon}\gt 25 GeV$
*  Photon ID MVA $\gt-0,9$

*  $\Delta R(jet, \gamma \gamma) 0,4$
*  $\Delta R(jet, \gamma ) \gt 0,4$
*  $\eta^{jet} \lt 4,7$
*  $p_T^{jet} \gt 20 GeV$


## $HH\longrightarrow bb\gamma \gamma$

here are listed the additional cuts implemented for the $HH\longrightarrow bb\gamma \gamma$ analysis

* b jets are chosen with the highest sum of b-tag score
* $\eta^{jet} \lt  2,5$
* $70 \lt  M_{jj} \lt  190$

# Variables 

We keep all the variables defined by the $H\longrightarrow \gamma \gamma$ analysis, and we add in addition : 

* Jets variables of the chosen jets ($p_T$ , $\eta$, $\phi$ , mass, Btag)
* The dijet variables of the H candidate ($p_T$ , $\eta$, $\phi$ , mass)
* The variables of the HH Candidate (chosen according to the selection above)
* $Cos(\theta^*_{CS})$ : Defined as the angle between the direction $H \longrightarrow \gamma\gamma$ candidate and the Colin-Supper reference frame.
* $Cos(\theta^*_{xx})$ defined as the angle between the particle $x$ and the direction given by $H\longrightarrow xx$, with $x$ being chosen randomly between the $x$'s , and $x= \gamma$ or $b$
* $\Delta R (\gamma, jet)$ and $\Delta R (\gamma, jet)_{min}$


